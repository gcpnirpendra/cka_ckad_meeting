## Lab requirements (detailed)
 - Participants need to have one cloud account (AWS/GCP/AZURE) to create kubernetes cluster.
 - 4 VMS with following specifications:
     - 8GB of RAM
     - 2 CPUs
     - 100 GB of Disk








## Case studies -Topics


### Case Study 1:
 #####  Deployment of 2 Tier Application:
 ######     Java Application
      - Tomcat
      - Mariadb
     
 #####  Steps:
    - Code is stored in Github.
    - Build tool is maven
    - Docker to create image  
    - Docker hub to store image
    - Scanning the images
    - Deployment of Application in prd-app namespace
    - Deployment of Database in prd-db namespace 

### Case Study  2:
  #####  Deployment of Different type database using kubernetes object statefulset.
    - Mariadb
    - Postgressql
    - Elasticsearch
### Case Study  3:
 #####  Deployment of Storage Solution of Different purposes.
    - Block storage
    - Object Storage
    - File storage
### Case Study  4:
  #####  Securing Application and Images
### Case Study  5:
 #####    Migration of Application from one cluster to another as per the need to achieve HA
### Case Study  6:
 #####    Scheduling the back up of etcd daily basis
### Case Study  7:
 #####   Monitoring and Logging 
      - Promethus setup (production Grade)
      - Grafana
      - AlertManager
### Case Study  8:
#####   Lift and Shift 
